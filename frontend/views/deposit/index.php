<?php

/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <p>
        <?php echo Html::a('Create Client', ['create'], ['class' => 'btn btn-success']);
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $model,
//         'filter'=>false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'created_at',
            [   'attribute' => 'client',
                'value' => function (\common\models\ClientDeposit $model) {
                    return $model->client->first_name . " " . $model->client->last_name;
                },
                'format' => 'raw',
            ],
            'client_id',
            'sum',
            'deposit_percent',
        ],
    ]); ?>
</div>
