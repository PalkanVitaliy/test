<?php

/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Add Client Deposit';
?>
<div class="site-index">
    <div class="col-sm-6">
        <?php $form = ActiveForm::begin(['class'=>'horizontal']); ?>
        <?php echo $form->field($model, 'sum')->textInput() ?>
        <?php echo $form->field($model, 'percent')->textInput() ?>
        <?php echo $form->field($model, 'client_id')->dropDownList($model->clientList()); ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-large btn-large-button btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
