<?php

/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Deposit History';
?>
<div class="site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $model,
//         'filter'=>false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'created_at',
            'deposit_id',
            'type',
            'old_sum',
            'new_sum',
        ],
    ]); ?>
</div>
