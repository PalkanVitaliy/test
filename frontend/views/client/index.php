<?php

/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Clients';
?>
<div class="site-index">
    <p>
        <?php echo Html::a('Create Client', ['create'], ['class' => 'btn btn-success']);
         ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $model,
//         'filter'=>false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'id',
            'created_at',
            'first_name',
            'last_name',
            'identification_number',
        ],
    ]); ?>
</div>
