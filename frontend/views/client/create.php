<?php

/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Add Client';
?>
<div class="site-index">
    <div class="col-sm-6">
        <?php $form = ActiveForm::begin(['class'=>'horizontal']); ?>
        <?php echo $form->field($model, 'first_name')->textInput() ?>
        <?php echo $form->field($model, 'last_name')->textInput(); ?>
        <?php echo $form->field($model, 'identification_number')->textInput(); ?>
        <?php echo $form->field($model, 'date_birth')->textInput(); ?>
        <?php echo $form->field($model, 'gender')->dropDownList($model->genderList()); ?>
        <?= Html::submitButton('Save', ['class' => 'btn btn-large btn-large-button btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
