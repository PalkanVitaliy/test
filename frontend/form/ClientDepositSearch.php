<?php


namespace frontend\form;


use common\models\Client;
use common\models\ClientDeposit;
use yii\data\ActiveDataProvider;

class ClientDepositSearch extends ClientDeposit
{
    /**
     * @inheritdoc
     */
    public function rules():array
    {
        return [
            [['id', 'client_id'], 'integer'],
            [['sum', 'percent', 'created_at'], 'safe'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientDeposit::find()->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}