<?php


namespace frontend\form;


use common\models\Client;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class DepositCreateForm extends Model
{
    public $percent;
    public $sum;
    public $client_id;

    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['percent', 'client_id'], 'integer'],
            ['sum', 'double']
        ];
    }

    public function clientList(): array
    {
        return ArrayHelper::map(
            Client::find()->asArray()->all(),
            'id',
            function($model) {
                return $model['first_name'].' '.$model['last_name'];
            }
        );
    }
}