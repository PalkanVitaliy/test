<?php
namespace frontend\form;

use common\models\Client;
use yii\data\ActiveDataProvider;

class ClientSearch extends Client
{
    /**
     * @inheritdoc
     */
    public function rules():array
    {
        return [
            [['id'], 'integer'],
            [['first_name', 'last_name', 'identification_number', 'gender', 'date_birth'], 'safe'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find()->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}