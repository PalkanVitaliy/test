<?php


namespace frontend\form;


use common\models\ClientDeposit;
use common\models\ClientDepositHistory;
use yii\data\ActiveDataProvider;

class ClientDepositHistorySearch extends ClientDepositHistory
{
    /**
     * @inheritdoc
     */
    public function rules():array
    {
        return [
            [['id', 'deposit_id'], 'integer'],
            [['sum_old', 'sum_new', 'type', 'created_at'], 'safe'],
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientDepositHistory::find()->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}