<?php


namespace frontend\form;


use common\models\Client;
use yii\base\Model;

class ClientCreateForm extends Model
{
    public $first_name;
    public $last_name;
    public $identification_number;
    public $gender;
    public $date_birth;



    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['first_name', 'last_name', 'identification_number', 'date_birth'], 'string', 'min' => 2, 'max' => 100],
           ['gender', 'integer']
        ];
    }

    public function genderList(): array
    {
        return [
            Client::GENDER_MAN => 'man',
            Client::GENDER_WOMAN => 'woman',
        ];
    }
}