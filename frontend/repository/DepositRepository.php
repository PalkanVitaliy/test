<?php


namespace frontend\repository;


use common\models\ClientDeposit;

class DepositRepository
{
    public function get($id): ClientDeposit
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(ClientDeposit $clientDeposit): void
    {
        if (!$clientDeposit->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }
    public function remove(ClientDeposit $clientDeposit): void
    {
        if (!$clientDeposit->delete()) {
            throw new \RuntimeException('Removing error.');
        }

    }

    private function getBy(array $condition): ClientDeposit
    {
        if (!$clientDeposit = \common\models\ClientDeposit::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('ClientDeposit not found.');
        }
        return $clientDeposit;
    }
}