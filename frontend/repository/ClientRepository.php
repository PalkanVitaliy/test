<?php
namespace frontend\repository;

use common\models\Client;

class ClientRepository
{
    public function get($id): Client
    {
        return $this->getBy(['id' => $id]);
    }

    public function save(Client $client): void
    {
        if (!$client->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }
    public function remove(Client $client): void
    {
        if (!$client->delete()) {
            throw new \RuntimeException('Removing error.');
        }

    }

    private function getBy(array $condition): Client
    {
        if (!$client = \common\models\Client::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Client not found.');
        }
        return $client;
    }
}