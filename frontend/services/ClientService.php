<?php
namespace frontend\services;

use common\components\Date;
use common\models\Client;
use common\models\ClientDeposit;
use frontend\form\ClientCreateForm;

class ClientService
{
    private $repository;
    private $transaction;

    public function __construct(\frontend\repository\ClientRepository $repository, TransactionManager $transaction)
    {
        $this->transaction = $transaction;
        $this->repository = $repository;
    }

    //Create new Client
    public function createClient(ClientCreateForm $form)
    {
        $user = Client::create($form);

        $this->repository->save($user);
    }
}