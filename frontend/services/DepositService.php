<?php

namespace frontend\services;

use app\models\ClientDepositHistory;
use common\components\Date;
use common\models\ClientDeposit;
use frontend\events\ClientEvent;
use frontend\form\DepositCreateForm;
use frontend\repository\DepositRepository;
use yii\base\Event;

class DepositService
{
    private $repository;
    private $transaction;

    public function __construct(DepositRepository $repository, TransactionManager $transaction)
    {
        $this->transaction = $transaction;
        $this->repository = $repository;
    }

    public function createDeposit(DepositCreateForm $form)
    {
        $deposit = ClientDeposit::create($form);

        $this->repository->save($deposit);
    }

    //Check expiration dates
    public static function checkExpirationData()
    {
        $events = new ClientEvent();

        $clientsExpirationProfit = ClientDeposit::find()->andWhere(['<', 'expiration_date_profit', (new Date())->now()])->all();

        if (!empty($clientsExpirationProfit)){
            foreach ($clientsExpirationProfit as $clientDeposit){
                $events->trigger(ClientEvent::EVENT_EXPIRATION_DATE_PROFIT, new Event(['sender' => $clientDeposit]));
            }
        }
    }

    public static function checkExpirationDataCommission()
    {
        $events = new ClientEvent();

        $clientsExpirationCommission = ClientDeposit::find()->andWhere(['<', 'expiration_date_commission', (new Date())->now()])->all();
        if (!empty($clientsExpirationCommission)){
            foreach ($clientsExpirationCommission as $clientDepositCommission){

                $events->trigger(ClientEvent::EVENT_EXPIRATION_DATE_COMMISSION, new Event(['sender' => $clientDepositCommission]));
            }
        }
    }
}