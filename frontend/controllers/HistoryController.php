<?php


namespace frontend\controllers;


use frontend\form\ClientDepositHistorySearch;
use Yii;
use yii\web\Controller;

class HistoryController extends Controller
{
    public function actionIndex()
    {
        $model = new ClientDepositHistorySearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', ['model'=>$model, 'dataProvider'=>$dataProvider]);
    }
}