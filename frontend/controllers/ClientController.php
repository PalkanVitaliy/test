<?php


namespace frontend\controllers;


use frontend\form\ClientCreateForm;
use frontend\form\ClientSearch;
use frontend\services\ClientService;
use Yii;
use yii\web\Controller;

class ClientController extends Controller
{
    private $service;

    public function __construct($id, $module, ClientService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $model = new ClientSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', ['model'=>$model, 'dataProvider'=>$dataProvider]);
    }

    public function actionCreate()
    {
        $form = new ClientCreateForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->createClient($form);
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }
}