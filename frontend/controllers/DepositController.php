<?php


namespace frontend\controllers;


use frontend\form\ClientDepositSearch;
use frontend\form\ClientSearch;
use frontend\form\DepositCreateForm;
use frontend\services\DepositService;
use Yii;
use yii\web\Controller;

class DepositController extends Controller
{
    private $service;

    public function __construct($id, $module, DepositService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }
    public function actionIndex()
    {
        $model = new ClientDepositSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', ['model'=>$model, 'dataProvider'=>$dataProvider]);
    }

    public function actionCreate()
    {
        $form = new DepositCreateForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->createDeposit($form);
                return $this->redirect(['index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }
}