<?php


namespace frontend\helpers;


class ComissionHelper
{
    public const FIRST = 5;
    public const SECOND = 6;
    public const THIRD = 7;

    public const RANGE_FIRST = 0;
    public const RANGE_SECOND = 1000;
    public const RANGE_THIRD = 10000;

    public const COMISSION_FIRST = 50;
    public const COMISSION_SEECOND = 5000;

    public static function getComission($sum)
    {
        if ($sum > self::RANGE_FIRST && $sum < self::RANGE_SECOND){
            $sumPercent = $sum * self::FIRST / 100;
            return ($sumPercent < self::COMISSION_FIRST) ? self::COMISSION_FIRST : $sumPercent;
        } elseif ($sum > self::RANGE_SECOND && $sum < self::RANGE_THIRD){
            return $sum * self::SECOND / 100;
        } elseif ($sum > self::RANGE_THIRD){
            $sumPercent = $sum * self::THIRD / 100;
            return ($sumPercent > self::COMISSION_SEECOND) ? self::COMISSION_SEECOND : $sumPercent;
        } else {
            return 0;
        }
    }
}