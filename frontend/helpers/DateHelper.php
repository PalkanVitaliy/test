<?php
namespace frontend\helpers;

use DateTime;

class DateHelper
{
    //Get expiration date with new date
    public static function getExpirationDateProfitForCreate()
    {
        return date("Y-m-d H:i:s", self::addMonth(time(), time()));
    }

    //Get expiration date from old date
    public static function getExpirationDateProfit($date, $created_at)
    {
        return date('Y-m-d H:i:s', self::addMonth(strtotime($date), strtotime($created_at)));
    }

    //Get expiration date commission
    public static function getExpirationDateCommissionForCreate()
    {
        $lastDay = new DateTime('last day of this month');
        $date = $lastDay->format('Y-m-d H:i:s');
        return $date;
    }

    public static function getExpirationDateCommission($date)
    {
        return date('Y-m-d H:i:s', self::addMonth(strtotime($date)));
    }

    //Get interval between dates
    public static function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);

    }

    //Add month
    public static function addMonth($time, $created_at = null)
    {
        $d=date('j',$time);
        $m=date('n',$time);
        $y=date('Y',$time);
        $h=date('h',$time);
        $i=date('i',$time);
        $s=date('s',$time);



        $m++;
        if ($m>12) { $y++; $m=1; }

        if ($d==date('t',$time)) {
            $d=31;
        }

        if (!checkdate($m,$d,$y)){
            $d=date('t',mktime(0,0,0,$m,1,$y));
        }

        if ($created_at){
            $d_created = date('j', $created_at);
            if ($d > $d_created){
                $d = $d_created;
            }
        }

        return mktime($h, $i, $s, $m, $d, $y);
    }
}