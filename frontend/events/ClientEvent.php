<?php
namespace frontend\events;

use common\models\ClientDepositHistory;
use common\models\ClientDeposit;

class ClientEvent extends \yii\base\Component
{
    const EVENT_EXPIRATION_DATE_PROFIT = 'Expiration Date Profit';
    const EVENT_EXPIRATION_DATE_COMMISSION = 'Expiration Date Commission';

    public function init()
    {
        $this->on(self::EVENT_EXPIRATION_DATE_PROFIT, [$this, 'expirationDateProfit']);
        $this->on(self::EVENT_EXPIRATION_DATE_COMMISSION, [$this, 'expirationDateCommission']);
    }

    //Deposit expiration date change event
    public function expirationDateProfit($event){
        $deposit = $event->sender;
        $oldSum = $deposit->sum;

        $deposit->updateProfit();
        $deposit->save();

        $this->saveHistory($deposit, $oldSum, ClientDepositHistory::PROFIT);
    }

    //Commission expiration date change event
    public function expirationDateCommission($event){
        $deposit = $event->sender;
        $oldSum = $deposit->sum;

        $deposit->updateCommission();
        $deposit->save();

        $this->saveHistory($deposit, $oldSum, ClientDepositHistory::COMISSION);
    }

    //Save History
    private function saveHistory(ClientDeposit $deposit, $oldsum, $type){
        $history = ClientDepositHistory::create($deposit, $oldsum, $type);
        $history->save();
    }
}