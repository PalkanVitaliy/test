#!/usr/bin/make
include .env

SHELL = /bin/sh

MYSQL_DUMPS_DIR=data/db/dumps

CURRENT_UID := $(shell id -u)
CURRENT_GID := $(shell id -g)

export CURRENT_UID
export CURRENT_GID

dstart:
	@env docker-compose up -d
dstop:
	@env docker-compose down
drestart: dstop dstart

mysql-restore-shop:
	@docker exec -i $(shell docker-compose ps -q mysqldb) mysql -u"$(MYSQL_ROOT_USER)" -p"$(MYSQL_ROOT_PASSWORD)" $(MYSQL_DATABASE) < $(MYSQL_DUMPS_DIR)/$(MYSQL_DATABASE).sql


mysql-create-tables:
	@docker exec -it $(shell docker-compose ps -q mysqldb) mysql -u"$(MYSQL_ROOT_USER)" -p"$(MYSQL_ROOT_PASSWORD)" -e "create database $(MYSQL_DATABASE_STAT)"

migrate:
	@docker exec -it $(shell docker-compose ps -q php) php yii migrate

composer:
	@docker exec -it $(shell docker-compose ps -q composer) install --ignore-platform-reqs --no-scripts

composer-up:
	@docker run --rm -v "$(shell pwd)":/app composer update --ignore-platform-reqs
