<?php

?>

<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <title>Персональный кондиционер «Arctic Air»</title>
    <meta name="keywords"
          content="Arctic Air, кондиционер Arctic Air, кондиционер Arctic Air купить, кондиционер Arctic Air цена, кондиционер Arctic Air отзывы, кондиционер Arctic Air заказать, кондиционер Arctic Air купить онлайн">
    <meta name="description" content="Кондиционер «Arctic Air». Закажите онлайн в нашем магазине с самой быстрой доставкой!">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=480">
    <meta name="theme-color" content="#262626"/>

    <link rel="shortcut icon" href='img/favicon-1a4.arb.ico' type="image/x-icon">
    <link rel="icon" href='img/favicon-1a4.arb.ico' type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Pattaya&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href='css/settings.css' type="text/css"/>
    <link rel="stylesheet" href='css/slick.css' type="text/css"/>
    <link rel="stylesheet" href='css/main.css' type="text/css"/>
    <script type="text/javascript" src='js/jquery.min.js'></script>

    <link rel="stylesheet" type="text/css" href='css/roboto.css'>
    <script src='js/jquery-d82.arb.js' type="text/javascript"></script>
    <script src='js/plugins.js' type="text/javascript"></script>
    <script src='js/detect.js' type="text/javascript"></script>

</head>
<body>
<h1>Hello</h1>


</body>
</html>