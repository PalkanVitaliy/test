<?php

namespace common\models;

use common\components\Date;
use frontend\form\ClientCreateForm;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bank_clients".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $identification_number
 * @property int|null $gender
 * @property string|null $date_birth
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property ClientDeposit[] $clientDeposit
 */
class Client extends \yii\db\ActiveRecord
{
    const GENDER_MAN = 0;
    const GENDER_WOMAN = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bank_clients';
    }

    public function behaviors():array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                //'value' => new Expression('NOW()'),
                'value' => (new Date())->now(),
            ],
        ];
    }

    //Create new Client
    public static function create(ClientCreateForm $form)
    {
        $client = new self();
        $client->first_name = $form->first_name;
        $client->last_name = $form->last_name;
        $client->identification_number = $form->identification_number;
        $client->date_birth = $form->date_birth;
        $client->gender = $form->gender;
        return $client;
    }

    /**
     * Gets query for [[BankClientsDeposits]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBankClientsDeposits()
    {
        return $this->hasMany(ClientDeposit::className(), ['client_id' => 'id']);
    }
}
