<?php

namespace common\models;

use app\models\ClientDepositHistory;
use common\components\Date;
use frontend\form\DepositCreateForm;
use frontend\helpers\ComissionHelper;
use frontend\helpers\DateHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bank_clients_deposit".
 *
 * @property int $id
 * @property int $client_id
 * @property float|null $sum
 * @property int|null $deposit_percent
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $expiration_date_profit
 * @property string|null $expiration_date_commission
 *
 * @property Client $client
 */
class ClientDeposit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bank_clients_deposit';
    }

    public function behaviors():array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                //'value' => new Expression('NOW()'),
                'value' => (new Date())->now(),
            ],
        ];
    }

    //Create new Deposit
    public static function create(DepositCreateForm $form): self
    {
        $deposit = new self();
        $deposit->client_id = $form->client_id;
        $deposit->deposit_percent = $form->percent;
        $deposit->sum = $form->sum;
        $deposit->expiration_date_profit = DateHelper::getExpirationDateProfitForCreate();
        $deposit->expiration_date_commission = DateHelper::getExpirationDateCommissionForCreate();
        return $deposit;
    }


     //Update Deposit Sum and Expiration Date
    public function updateProfit()
    {
        $this->sum += $this->sum * $this->deposit_percent / 100;
        $this->expiration_date_profit = DateHelper::getExpirationDateProfit($this->expiration_date_profit, $this->created_at);
    }


     //Update Deposit Commission and Expiration Date
    public function updateCommission()
    {
        $countDays =  DateHelper::dateDifference($this->created_at, $this->expiration_date_commission);
        $dayOfLastMonth = date('t', strtotime($this->expiration_date_commission));
        $countDays = ($dayOfLastMonth > $countDays) ? $dayOfLastMonth : $countDays;

        $commission = ComissionHelper::getComission($this->sum);

        $this->sum -= $countDays * $commission / $dayOfLastMonth;
        $this->expiration_date_commission = DateHelper::getExpirationDateCommission($this->expiration_date_commission);
    }

    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

}
