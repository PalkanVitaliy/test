<?php

namespace common\models;

use common\components\Date;
use common\models\ClientDeposit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "client_deposit_history".
 *
 * @property int $id
 * @property int $deposit_id
 * @property string $type
 * @property string $old_sum
 * @property int|null $new_sum
 * @property string|null $created_at
 *
 * @property ClientDeposit $deposit
 */
class ClientDepositHistory extends \yii\db\ActiveRecord
{
    public const PROFIT = 'profit';
    public const COMISSION = 'comission';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_deposit_history';
    }

    public function behaviors():array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                //'value' => new Expression('NOW()'),
                'value' => (new Date())->now(),
            ],
        ];
    }

    //Create new history deposit
    public static function create(ClientDeposit $deposit, $oldSum, $type)
    {
        $history = new self();
        $history->deposit_id = $deposit->id;
        $history->old_sum = $oldSum;
        $history->new_sum = $deposit->sum;
        $history->type = $type;
        return $history;
    }


    /**
     * Gets query for [[Deposit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeposit()
    {
        return $this->hasOne(ClientDeposit::className(), ['id' => 'deposit_id']);
    }
}
