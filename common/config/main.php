<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.$_SERVER['MYSQL_HOST'].';port=3306;dbname='.$_SERVER['MYSQL_DATABASE'],
            'username' => $_SERVER['MYSQL_ROOT_USER'],
            'password' => $_SERVER['MYSQL_ROOT_PASSWORD'],
            'charset' => 'utf8',
            //'enableSchemaCache' => true,
        ],
    ],
];
