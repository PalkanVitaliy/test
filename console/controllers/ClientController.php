<?php
namespace console\controllers;

use frontend\services\ClientService;
use frontend\services\DepositService;
use yii\console\Controller;

class ClientController extends Controller
{
    public function actionIndex()
    {
        $this->stdout('Start!' . PHP_EOL);

        DepositService::checkExpirationData();

        $this->stdout('done!' . PHP_EOL);
    }

    public function actionCommission()
    {
        $this->stdout('Start!' . PHP_EOL);

        DepositService::checkExpirationDataCommission();

        $this->stdout('done!' . PHP_EOL);
    }
}