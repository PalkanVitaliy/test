<?php

use yii\db\Migration;

/**
 * Class m200706_053700_update_column_history
 */
class m200706_053700_update_column_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%client_deposit_history}}', 'old_sum', $this->decimal(10,2));
        $this->alterColumn('{{%client_deposit_history}}', 'new_sum', $this->decimal(10,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
