<?php

use yii\db\Migration;

/**
 * Class m200702_101521_create_new_table_history
 */
class m200702_101521_create_new_table_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%client_deposit_history}}', [
            'id' => $this->primaryKey(),
            'deposit_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'old_sum' => $this->string()->notNull(),
            'new_sum' => $this->smallInteger(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('{{%idx-client_deposit_history-deposit_id}}', '{{%client_deposit_history}}', 'deposit_id');
        $this->addForeignKey('{{%fk-client_deposit_history-deposit_id}}', '{{%client_deposit_history}}', 'deposit_id', '{{%bank_clients_deposit}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_deposit_history}}');
    }
}
