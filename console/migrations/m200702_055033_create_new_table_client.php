<?php

use yii\db\Migration;

/**
 * Class m200702_055033_create_new_table_client
 */
class m200702_055033_create_new_table_client extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%bank_clients}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'identification_number' => $this->string()->notNull(),
            'gender' => $this->smallInteger(),
            'date_birth' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ], $tableOptions);

        $this->createTable('{{%bank_clients_deposit}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'sum' => $this->decimal(10,2),
            'deposit_percent' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'expiration_date_profit' =>  $this->dateTime(),
            'expiration_date_commission' =>  $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('{{%idx-bank_clients_deposit-client_id}}', '{{%bank_clients_deposit}}', 'client_id');
        $this->createIndex('{{%idx-bank_clients_deposit-expiration_date_commission}}', '{{%bank_clients_deposit}}', 'expiration_date_commission');
        $this->createIndex('{{%idx-bank_clients_deposit-expiration_date_profit}}', '{{%bank_clients_deposit}}', 'expiration_date_profit');
        $this->addForeignKey('{{%fk-bank_clients_deposit-client_id}}', '{{%bank_clients_deposit}}', 'client_id', '{{%bank_clients}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bank_clients}}');
        $this->dropTable('{{%bank_clients_deposit}}');
    }
}
